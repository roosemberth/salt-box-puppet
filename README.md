# box-puppet

A simple tool to run some basic operations on my salt box in CLI-mode.

See `stack run -- --help` for details.

This tool is known to be working in a salt box with the following features:

- Model Name: Salt_Fiber_Box
- Firmware Version: v1.01.38 build105
- Boot Code Version: 0.00.01
- Hardware Version: R01B
