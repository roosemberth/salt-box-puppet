module Main where
import BoxPuppet.Actions   (ping)
import BoxPuppet.Commands
  ( PingCommand (..)
  , PingCount (..)
  , PingIface (..)
  , PingProto (..)
  , PingResult (..)
  )
import BoxPuppet.Session   (LoginData (LoginData))
import Control.Monad.Fail  (fail)
import Network.HTTP.Req
  (MonadHttp, Scheme (Http), Url, defaultHttpConfig, http, runReq)
import Options.Applicative (Parser, help, idm, info, metavar)
import Protolude

import qualified Options.Applicative as OA

newtype FailToStdErrT m a = FailToStdErrT { runFailToStdErr :: m a }

deriving instance Functor m     => Functor (FailToStdErrT m)
deriving instance Applicative m => Applicative (FailToStdErrT m)
deriving instance Monad m       => Monad (FailToStdErrT m)
deriving instance MonadIO m     => MonadIO (FailToStdErrT m)
deriving instance MonadHttp m   => MonadHttp (FailToStdErrT m)

instance MonadIO m => MonadFail (FailToStdErrT m) where
  fail msg = hPutStrLn stderr msg >> liftIO (throwIO (AssertionFailed ""))

data GlobalOpts = GlobalOpts
  { host     :: Url 'Http
  , username :: Text
  , password :: Text
  }

sendPing :: GlobalOpts -> PingOpts -> IO ()
sendPing GlobalOpts{..} PingOpts{..} = do
  (PingResult r) <- runReq defaultHttpConfig . runFailToStdErr $
    ping host (LoginData username password) (PingCmd target count proto iface)
  putStrLn r

data PingOpts = PingOpts
  { target :: Text
  , count  :: PingCount
  , proto  :: PingProto
  , iface  :: PingIface
  }

newtype Options = Options PingOpts

cmdParser :: GlobalOpts -> Options -> IO ()
cmdParser gOpts (Options pingOpts) = sendPing gOpts pingOpts

pingParser :: Parser (GlobalOpts -> IO ())
pingParser = flip cmdParser . Options <$> (PingOpts
  <$> OA.strArgument
      (  metavar "target"
      <> help "Where to send the ping packets to."
      )
  <*> (   OA.flag' Continous
            (  OA.long "continuous"
            <> help "Send continuous ping packets."
            )
      <|> OA.option (fmap Fixed OA.auto)
            (  OA.long "count"
            <> help "How many ping packets to send."
            <> OA.value (Fixed 1)
            <> OA.showDefault
            )
      )
  <*> (   OA.flag' IPv6
            ( OA.short '6'
            <> help "Force pinging using IPv6."
            )
      <|> OA.flag' IPv4
            ( OA.short '4'
            <> help "Force pinging using IPv4."
            )
      <|> pure AnyProto
      )
  <*> OA.flag Wan Lan
        ( OA.long "lan"
        <> help "Send packet to the Local network instead."
        )
  )

parseUrl :: [Char] -> Maybe (Url 'Http)
parseUrl t | null t       = Nothing
parseUrl t | '/' `elem` t = Nothing
parseUrl t = Just . http . toS $ t

opts :: Parser (IO ())
opts = OA.subparser
  ( OA.command "ping" (info
      (pingParser <**> OA.helper)
      (OA.progDesc "Ping a remote host."))
  ) <*>
  ( GlobalOpts
    <$> OA.option (OA.maybeReader parseUrl) (
      OA.long "host"
      <> help "Hostname or IP address of the box."
    ) <*> OA.strOption (
      OA.long "user"
      <> help "Username to login as."
      <> OA.value "admin"
      <> OA.showDefault
    ) <*> OA.strOption (
      OA.long "pass"
      <> help "Password to login as."
    )
  )

main :: IO ()
main = join $ OA.customExecParser p (info (opts <**> OA.helper) idm)
  where p = OA.prefs (OA.showHelpOnEmpty <> OA.showHelpOnError)
