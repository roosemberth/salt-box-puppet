module BoxPuppet.Actions where

import BoxPuppet.Commands
import BoxPuppet.Session
import Control.Monad       (fail)
import Control.Monad.Trans (MonadTrans)
import Data.Time.Clock     (getCurrentTime)
import Data.Time.Format    (defaultTimeLocale, formatTime)
import Network.HTTP.Client (CookieJar)
import Network.HTTP.Req
import Protolude
import Text.HTML.TagSoup
import Text.StringLike     (StringLike)

import qualified Data.ByteString as BS
import qualified Data.Text       as T
import           Data.Time       (UTCTime)

type BoxUrl = Url 'Http

extractImgSrcs :: (Monoid s, Show s, StringLike s) => s -> [s]
extractImgSrcs = filter (/= mempty)
  . fmap (fromAttrib "src")
  . filter isTagOpen
  . mapMaybe head
  . sections (~== ("<img>" :: [Char]))
  . parseTags

ttFromMagicImage :: (MonadFail m, MonadIO m) => ByteString -> m Token
ttFromMagicImage magicImageBS = do
  magicImage <- either
    (\e -> fail $ "Could not decode request body" <> show e)
    pure
    (decodeUtf8' magicImageBS)
  maybe
    (fail "Could not construct token.")
    pure
    (tokenFromImg magicImage)

ttFromHtmlPage :: (MonadIO m, MonadFail m) => ByteString -> m Token
ttFromHtmlPage = maybe
  (fail "Could not find magic image to calculate token.")
  ttFromMagicImage
  . head . filter (BS.isPrefixOf "data:") . extractImgSrcs

getToken :: (MonadIO m, MonadFail m, MonadHttp m)
  => BoxUrl -> Network.HTTP.Req.Option Http -> m Token
getToken url opts =
  ttFromHtmlPage . responseBody =<< req GET url NoReqBody bsResponse opts

class ToOption a where
  toOption :: a -> forall scheme. Network.HTTP.Req.Option scheme

instance ToOption Token where
  toOption (Token t) = "_tn" =: t

newtype ReqTime = ReqTime UTCTime

instance ToOption ReqTime where
  toOption (ReqTime t) = "t" =: formatTime defaultTimeLocale "%s000" t

data Referer = forall scheme. Referer (Url scheme)

instance ToOption Referer where
  toOption (Referer url) = header "Referer" (encodeUtf8 $ renderUrl url)

login :: (MonadIO m, MonadFail m, MonadHttp m)
  => BoxUrl -> LoginData -> m (Token, CookieJar)
login url loginData = do
  token <- getToken url mempty
  let payload = mconcat $ uncurry (=:) <$> toLoginPayload loginData token
      loginUrl = url /: "login.cgi"
  res <- req POST loginUrl (ReqBodyUrlEnc payload) ignoreResponse
         (toOption $ Referer loginUrl)
  let cj = responseCookieJar res
  token <- getToken (url /: "index.htm") (cookieJar cj)
  pure (token, cj)

class Monad m => WithSessionCtx m where
  -- | Http Options attached to the current session.
  ctxHttpOpts :: m (Network.HTTP.Req.Option Http)
  -- | Get the current value of the token. This may change without notice.
  peekToken :: m Token
  -- | Refreshes the token in the session using the specified page.
  refreshToken :: Url Http -> m ()

data SessionCtx = SessionCtx Token CookieJar

newtype SessionCtxT m a = SessionCtxT { runSession :: StateT SessionCtx m a }
  deriving (Functor, Applicative, Monad)
  deriving newtype (MonadState SessionCtx, MonadTrans, MonadFail)

instance (MonadIO m, MonadHttp m, MonadFail m)
  => WithSessionCtx (SessionCtxT m) where
  ctxHttpOpts = SessionCtxT $ do
    (SessionCtx token cj) <- get
    pure (cookieJar cj <> toOption token)
  peekToken = get >>= \(SessionCtx t _) -> pure t
  refreshToken url = do
    time <- ReqTime <$> liftIO getCurrentTime
    opts <- ctxHttpOpts
    (SessionCtx _ cj) <- get
    getToken url (opts <> toOption time) >>= \t -> put (SessionCtx t cj)

deriving instance MonadIO m => MonadIO (SessionCtxT m)

instance MonadHttp m => MonadHttp (SessionCtxT m) where
  handleHttpException = lift . handleHttpException

mkSession :: forall m a . (MonadIO m, MonadFail m, MonadHttp m)
  => BoxUrl -> LoginData -> SessionCtxT m a -> m a
mkSession url loginData sm = do
  s <- login url loginData <&> uncurry SessionCtx
  fst <$> runStateT (runSession sm) s

ping :: forall m . (MonadIO m, MonadFail m, MonadHttp m)
  => BoxUrl -> LoginData -> PingCommand -> m PingResult
ping url loginData pingCmd = mkSession url loginData $ do
  let diagToolUrl = url /: "diag_tool.htm"
  refreshToken diagToolUrl
  token <- peekToken
  opts <- ctxHttpOpts
  void $ req POST (url /: "networktools.cgi")
    (ReqBodyUrlEnc . mconcat $ uncurry (=:) <$> toNettoolPayload pingCmd token)
    ignoreResponse
    (opts <> toOption (Referer diagToolUrl))
  result <- whileNothing extractOutput $ do
    time <- ReqTime <$> liftIO getCurrentTime
    res <- req GET (url /: "cgi" /: "cgi_cmdRet_check.js") NoReqBody bsResponse
      (opts <> toOption (Referer diagToolUrl) <> toOption time)
    either
      (\e -> fail $ "Could not decode response body: " <> show e)
      pure
      (decodeUtf8' $ responseBody res)
  pure (PingResult result)
  where
    whileNothing :: Monad m1 => (a -> Maybe a) -> m1 a -> m1 a
    whileNothing pred f = f >>= go . pred
      where go Nothing  = whileNothing pred f
            go (Just v) = pure v
    extractOutput = fmap unlines . acceptable . fmap classify . lines
      where classify v = maybeToRight v (T.stripPrefix "ILHHJ" v)
            acceptable v | length (rights v) /= 2 = Nothing
            acceptable v = Just $ fmap (either identity identity) v
