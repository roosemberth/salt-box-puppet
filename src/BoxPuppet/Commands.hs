module BoxPuppet.Commands where

import BoxPuppet.Session (Token (..))
import Protolude

data PingProto = IPv4 | IPv6 | AnyProto

instance ConvertText PingProto Text where
  toS IPv4     = "v4"
  toS IPv6     = "v6"
  toS AnyProto = "any"

data PingCount = Continous | Fixed Integer
  deriving Show

instance ConvertText PingCount Text where
  toS Continous = "count"
  toS (Fixed i) = Protolude.show i

data PingIface = Wan | Lan

instance ConvertText PingIface Text where
  toS Wan = "wan"
  toS Lan = "lan"

data PingCommand = PingCmd
  { dest  :: Text
  , count :: PingCount
  , proto :: PingProto
  , iface :: PingIface
  }

-- | The result of a ping command.
-- Minimalistic representation after the whole operation has completed.
-- This means that continuous pings will never yield a ping result.
-- This type should evolve into a streaming type eventually.
newtype PingResult = PingResult Text
  deriving stock Show

toNettoolPayload :: PingCommand -> Token -> [(Text, Text)]
toNettoolPayload PingCmd{..} (Token httoken) =
  [ ("httoken", httoken)
  , ("dest"   , dest)
  , ("cmd"    , "ping")
  , ("count"  , toS count)
  , ("proto"  , toS proto)
  , ("inf"    , toS iface)
  ]
