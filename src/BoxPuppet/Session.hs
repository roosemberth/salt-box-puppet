module BoxPuppet.Session where

import Crypto.Hash
import Data.Text.Encoding.Base64 (decodeBase64)
import Protolude                 hiding (hash)

import qualified Data.Text as T

newtype Token = Token Text
  deriving stock (Eq, Show)

instance ConvertText Token Text where
  toS (Token t) = t

data LoginData = LoginData
  { username :: Text
  , password :: Text
  }

toLoginPayload :: LoginData -> Token -> [(Text, Text)]
toLoginPayload LoginData{..} (Token httoken) =
  [ ("httoken", httoken)
  , ("pws", weirdSecurityEncode password)
  , ("usr", weirdSecurityEncode username)
  ]
  where
    weirdSecurityEncode =
      show . hash @SHA512 . encodeUtf8 . show . hash @MD5 . encodeUtf8

-- | Takes an img src attribute that has been determined to contain the token.
tokenFromImg :: Text -> Maybe Token
tokenFromImg t = if T.length t > 78 then mkToken t else Nothing
  where mkToken = fmap Token . rightToMaybe . decodeBase64 . T.drop 78
