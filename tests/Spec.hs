{-# LANGUAGE TemplateHaskell #-}

import BoxPuppet.Actions
import BoxPuppet.Session
import Data.FileEmbed    (embedFile)
import Protolude
import System.Exit       (exitFailure)
import Test.QuickCheck

prop_encodeKnownUserPass :: Property
prop_encodeKnownUserPass = toLoginPayload loginData (Token token) === value
  where
    value = [ ("httoken", token), ("pws", pws), ("usr", usr) ]
    loginData = LoginData "admin" "testpassword"
    token = "1367740042"
    pws :: Text = "bbd59e55783c4008979ac24820be84c7b6fd3727a4958766b0ba84005f6ac382c9652adccba3ddae0cd16329e121d5fee1793af60f8eac225e7e98884fca7cae"
    usr :: Text = "edbd881f1ee2f76ba0bd70fd184f87711be991a0401fd07ccd4b199665f00761afc91731d8d8ba6cbb188b2ed5bfb465b9f3d30231eb0430b9f90fe91d136648"

prop_tokenFromImg :: Property
prop_tokenFromImg = Just token === tokenFromImg img
  where
    img = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7MzEyNjg2OQ=="
    token = Token "3126869"

prop_tokenFromHtml :: Property
prop_tokenFromHtml = ioProperty $ do
  token <- ttFromHtmlPage $(embedFile "resources/page-for-tests.html")
  pure $ token === Token "2090448024"

-- needed on GHC 7.8 and later; without it, quickCheckAll will not be able to
-- find any of the properties.
return []

main :: IO ()
main = unlessM $quickCheckAll exitFailure
